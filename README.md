# Pipette Stands

**Project Status:** Prototype, work in progress.

Parametrics design for stands for "Gilson-style" single-channel micropipettes that can be cut from acrylic using a laser cutter or similar device. There is also an alternative design in case you have an acrylic bender. The width of acrylic can be set, as can the number of pipettes. Other parameters can also be varied.

![Laser cut design](./images/laser-cut-stand.jpg)

<img src="./images/stand-with-pipettes.jpg"  width="500" >

## Instructions for laser cutting

### Ready-to-cut files

If you are using 5 mm acrylic, there are svg and dxf designs for 3-5 micropipettes ready to download in the [svg](./sgv/) and [dxf](./dxf/) folders

1) dxf files can typically be loaded directly into your laser cutter software, or if you want to add personalisation such as lab logos, names etc then you can use the SVG, edit in Inkspace or a similar SVG editor and export as DXF from there to import into your laser cutter software.
2) before cutting the whole design, cut the finger joint test piece to ensure a snug fit based on you particular acrylic and laser cutter precision, kerf and tolerances. If the sizing is incorrect, you can use the OpenSCAD file to customise it.
3) assemble the stand following the images in this repository if needed.
4) apply acrylic cement following the manufacturer's instructions to ensure the parts are permanently bonded.

### Customisable OpenSCAD files

1) Change the personalisable parameters: pipette_qty (number of single-channel "Gilson-style" micropipettes that the stand can hold) and acrylic_thickness (thickness of acrylic - recommended to measure the sheet you will cut from with calipers). Other parameters can also be adjusted to fit different styles of pipettes, you will need 
2) Render in OpenSCAD. By default all parts are rendered together, if you want to render individual parts you can comment out the ones you don't need but it is probably easier to just delete them in you laser cutter software software
3) Export a DXF file and load directly into your laser cutter software, or if you want to add personalisation such as lab logos, names etc then you can export as SVG, edit in Inkspace or a similar SVG editor and export as DXF from there to import into your laser cutter software.abs
4) Laser cut using the acrylic width defined in the personalisable parameters. Before cutting the whole design, cut the finger joint tets piece to ensure a snug fit based on you particular acrylic and laser cutter precision, kerf and tolerances. Adjust paramteters and render again is the sizing is incorrect.
5) Assemble the stand following the images in the gitlab repository if needed.
6) Apply acrylic cement following the manufacturer's instructions to ensure the parts are permanently bonded.

## License
Designed in OpenSCAD by Jenny Molloy (jcm80@cam.ac.uk, University of Cambridge, UK) based on an original design by Prince Samoh (Hive Biolab, Kumasi Hive, Ghana). Copyright Open Bioeconomy Lab 2021, licensed under the CERN OHL-P (https://ohwr.org/cern_ohl_p_v2.txt)

## Contributing
If you would like to contribute new design improvements, documentation updates or parameters for different types of pipettes, raise an issue in the repository or email jcm80@cam.ac.uk.

All improvements and suggestions are welcome! Please follow our [Code of Conduct](./Code_of_Conduct.md) for all contributions and communications within the project.


