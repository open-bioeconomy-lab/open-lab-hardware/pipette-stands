// PARAMETRIC PIPETTE STAND: FINGER JOINTS //
// 
// Description: Stands for "Gilson-style" single-channel micropipettes that can be cut from acrylic using a laser cutter or similar device. There is also an alternative design in case you havean acrylic bender. The width of acrylic can be set, as can the number of pipettes. Other parameters can also be varied.

// HOW TO USE THIS FILE //
// 1) Change the personalisable parameters below. We recommend measuring your acrylci with calipers to be sure of the thickness
// 2) Render in OpenSCAD. By default all parts are rendered together, if you want to render individual parts you can comment out the ones you don't need but it is probably easier to just delete them in you laser cutter software software
// 3) Export a DXF file and load directly into your laser cutter software, or if you want to add personalisation such as lab logos, names etc then you can export as SVG, edit in Inkspace or a similar SVG editor and export as DXF from there to import into your laser cutter software.abs
// 4) Laser cut using the acrylic width defined in the personalisable parameters. Before cutting the whole design, cut the finger joint tets piece to ensure a snug fit based on you particular acrylic and laser cutter precision, kerf and tolerances. Adjust paramteters and render again is the sizing is incorrect.
// 5) Assemble the stand following the images in the gitlab repository if needed.
// 6) Apply acrylic cement following the manufacturer's instructions to ensure the parts are permanently bonded.

// PERSONALISABLE PARAMETERS - PLEASE CHANGE THESE //
// Number of single-channel "Gilson-style" micropipettes that the stand can hold
pipette_qty = ;
// Thickness of acrylic - recommended to measure the sheet you will cut from with calipers.
acrylic_thickness = 5;


// OTHER PARAMETERS - CHANGE THESE WITH CAUTION //
// Overall width of stand 
stand_width = ((pipette_qty*30)+15);
// Depth of base, may need adjusting to match centre of gravity and avoid the stand falling over
stand_base = 100;
// Overall height of stand
stand_height = 260;
//Width of the cut-out to hold the micropipette, must be narrower than the micropipette
holder_width = 21;
holder_depth = 40;
holder_shelf_depth = 30;

// COMPONENT MODULES //

// Main body of the pipette stand
module stand(){
    square(size = [stand_width, stand_height], center = false);
}

// Base of the pipette stand
module base(){
    square(size = [stand_width, stand_base], center = false);
}

// Finger joint slot
module slot(){
    square([5,acrylic_thickness],center = false);
}

// Cut out holder for one single-channel "Gilson-style" micropipette
module holder(){
    resize([holder_width,holder_depth])circle(d=holder_width);
}

// PART MODULES //

// Stand with one row of finger slots for lower pipette holder and an upper pipette holder to be produced by bending the acrylic. Stand is produced by acrylic bending

module stand_with_slots(){
difference(){
offset(16) offset(-16) stand();
for (i=[1:3*pipette_qty])  {
	translate([(10*i),25.88,0]) slot();
    translate([(10*i),140,0]) slot();
    translate([(10*i),230,0]) slot();
}
}
}

module base_with_fingers(){
union(){
offset(6) offset(-6)  base();
for (i=[1:3*pipette_qty])  {
	translate([(10*i),-acrylic_thickness,0]) slot();
}
}
}

module holder_with_fingers(){
difference(){
union(){
square(size = [stand_width, (holder_shelf_depth+acrylic_thickness)], center = false);
for (i=[1:3*pipette_qty])  {
	translate([(10*i),-acrylic_thickness,0]) 
    slot();
}
}
for (i=[1:pipette_qty])  {
translate([(i*(stand_width/pipette_qty)-(stand_width/pipette_qty)/2),(holder_depth-10),0]) 
    holder();
}
}
}

// FINGER JOINT TEST MODULE // 
// Use this to test the fit of the finger joints on a small square of acrylic before cutting the whole stand

module finger_joint_test(){
difference(){
union(){
// Sized to be six times the width of the acrylic to enable three joints to be tested
square(size = 6*acrylic_thickness,6*acrylic_thickness, center = false);   
for (i=[1:3])  {
translate([6*acrylic_thickness,5+(i-1)*8,0]) square([5,acrylic_thickness],center = false);
}}
for (i=[1:3])  {
translate([10,5+(i-1)*8,0]) square([5,acrylic_thickness],center = false);
}
}
}

// RUN MODULES //
stand_with_slots();
translate([0,stand_height+10,0]) base_with_fingers();
translate([0,1.05*(stand_height+stand_base),0]) holder_with_fingers();
translate([0,1.05*(stand_height+stand_base+holder_depth),0]) holder_with_fingers();
translate([0,1.05*(stand_height+stand_base+(2*holder_depth)),0])  finger_joint_test();

// END //





